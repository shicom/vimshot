/*gaem開始の判断*/
var gamestart = false;

/*カウント用の変数*/
var countD = 3;

/*元のコード*/
var stage_code = $('#stagecode').text();

/*スコア*/
var score = 0;

/*タイマ*/
var time = 120;

/*ランダムな整数15個格納するよう*/
var randomNum = [];
randomNum.push(20);

/*main関数*/
$(function(){
  
  /*gamestart!*/
  $('#startbutton').on('click', function(){
    gamestart = true;

    /*ヨーイドンpopup*/
    startpop(gamestart);
    
    /*gameplay*/
    setTimeout(play, 3000);

  });
});



/*スタートpop表示*/
function startpop(gamestart){
  if (!gamestart) return false;
  
  $('#startpop').fadeIn();
  document.getElementById("startpop__count").innerHTML = countD; 

  /*カウントダウン関数呼び出し*/
  setTimeout(countdown, 1000);
}



/*カウントダウン*/
var countdown = function(){
  countD--;
  if(countD == 0){
    document.getElementById("startpop__count").innerHTML = "START!";
    $('#startpop').fadeOut(2000);
    $('#startbutton').off();
    
  } else {
    setTimeout(countdown, 1000);
    document.getElementById("startpop__count").innerHTML = countD;
  }
}

/*画面中央にモーダルを追加*/
function modalResize(){
  var w = $("body").width();
  var h = $("body").height();
  var cw = $('#score_show').width();
  var ch = $('#score_show').height();

  $("#score_show").css({
    "left": (( w - cw )/2) + "px",
    "top": (( h - ch )/2) + "px"
  });


}

/*タイマー*/
function timer(){
  time--
  if(time == 0){
    /*タイムを０にしてspanをクリックできなくする*/
    document.getElementById("timer").innerHTML = "<p id = \"timer\">" + "time:" + time + "</p>";
    $("span").off();

    /*点数表示*/
    modalResize();
    document.getElementById("score_show").innerHTML = "<p>" + "SCORE:" + score + "</p>" + '<p id = "close_score">閉じる</p>';
    $("body").append('<div id="score_show_bg"></div>');
    $("#score_show").fadeIn();
    $("#close_score").click(function(){
      $("#score_show").fadeOut();
      $("#score_show_bg").remove();
    });


    gamestart = false;
    return;
  } else if(time >0){
  document.getElementById("timer").innerHTML = "<p id = \"timer\">" + "time:" + time + "</p>";
  setTimeout(timer,1000);
  }
}


/*ゲームプレイ*/
var play = function(){
  
  /*点数初期化*/
  score = 0;
  
  /*タイマーセット*/
  time = 120;

  /*的をセット*/
  set_mato();
  timer();
  
  
}





/*文字挿入関数*/
function insertStr(str, index, insert){
 var first = str.slice(0, index);
 var latter = str.slice(index, str.length);
 return first + insert + latter;
}




/*htmlエスケープ*/
function escape_html(string){
  if(typeof string !== 'string'){
    return string;
  }
  return string.replace(/[&'`"<>]/g, function(match) {
    return {
      '&': '&amp;',
      "'": '&#x27;',
      '`': '&#x60;',
      '"': '&quot;',
      '<': '&lt;',
      '>': '&gt;',
    }[match]
  });
}


/*<spam>を戻す*/
function escape_span(string){
  if(typeof string !== 'string'){
    return string;
  }
  return string.replace(/(&lt;span&gt;B&lt;\/span&gt;)/g, function(match) {
    return {
      '&lt;span&gt;B&lt;/span&gt;': "<span>B</span>",
    }[match]
  });
}


function set_mato(){
  /*乱数セット*/
  for(var i = 1; i <= 14; i++){
    randomNum[i] = randomNum[i-1] + Math.floor(Math.random()*((stage_code.length -20)/13 - ((stage_code.length -20)/13 - 5)) + ((stage_code.length -20)/13 - 5));
  }

  /*的セット*/
  var stage_code_modify = stage_code;
  for (var i = 0; i <=14; i++){
    stage_code_modify = insertStr(stage_code_modify, randomNum[i], "<span>B</span>");
  }
  stage_code_modify = escape_html(stage_code_modify);
  stage_code_modify = escape_span(stage_code_modify);

  /*的を表示*/
  document.getElementById("stagecode").innerHTML= stage_code_modify;

  $("span").on('click', function(){
    $(this).remove();
  });

}
