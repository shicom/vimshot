class StagesController < ApplicationController
  before_action :logged_in_user 

  def new 
    @stage = Stage.new
  end

  def create
    @stage = Stage.new(stage_params)

    if @stage.save
    end

  end

  def edit 
    @stage = Stage.find(params[:id])
  end

  def update
    @stage = Stage.find(params[:id])
    if @stage.update_attributes(stage_params)
    end
  end

  def destroy
  end

  def index
    @stages = Stage.all
  end

  def show
    @stage = Stage.find(params[:id])
  end


  private

    def stage_params
      params.require(:stage).permit(:name, :level, :code)
    end

    def logged_in_user
      unless logged_in?
        redirect_to root_path
      end
    end








  
end
