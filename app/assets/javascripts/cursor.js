/*行サイズ*/
var lsize = 20;

/*列サイズ*/
var rsize = 12;

/*改行の正規表現gフラグつき*/
var regular_kaigyou = /\r\n|\n|\r/g; 

/*wordの正規表現*/
var regular_word = /[^\s\w]+|\w+/g;

/*空白の連続と任意の一文字の正規表現*/
var regular_kuuhaku = /\s+./;

/*カーソル上移動の時、現在位置を一時保持する*/
var posmemory = null;

/*gg,88g,変数*/
var comand = [];

/*game.jsにカーソルの座標を渡す*/
var cursor_position 

/*的の数*/
var mato = 15;


/*ここからカーソル操作*/
$(function(){
  $(window).keydown(function(e){

    /*問題コード（ステージ)*/
    var codetext = $('#stagecode').text();

    /*一行ごとに配列に格納*/
    var linecode = codetext.split(/\r\n|\n|\r/);
    linecode.unshift(0);

    var keycode = e.keyCode;
    /*カーソルの座標（相対),行,列*/
    var p = $('.cursor').position();
    var line = p.top/lsize + 1;
    var row = p.left/rsize + 1;
    cursor_position = p;

    /*コードの行数*/
    var linecount = codetext.match(regular_kaigyou).length + 1;

    /*改行コードの位置（文字列に相対して）*/
    var nposition = [];
    nposition.push(-1);
    var result;
    while(result = regular_kaigyou.exec(codetext)){
      nposition.push(result.index); 
    }
    nposition.push(codetext.length);

    /*カーソル位置の文字のインデックス*/
    var pindex = nposition[line - 1] + row;

    /*各行の末尾の列の位置*/
    var end_row_position = []
    end_row_position.push(0);
    for (var i = 0; i < linecount; i++){
      var l = nposition[i+1]-nposition[i]-1;
      end_row_position.push(l);
    }


    /*行の先頭の単語の列の位置*/
    var head_row_position = [];
    head_row_position.push(0);
    for (var i = 1; i<= linecount; i++){
      head_row_position.push(linecode[i].search(/\S/)+1);
    }

    /*行のwordのインデックスw用,e用*/
    var wordindex = [];
    var next_wordindex = [];
    var back_wordindex = [];
    var e_wordindex = [];
    var e_next_wordindex = [];
    var e_back_wordindex = [];
    
    word_index_set(wordindex, e_wordindex, line, linecode,  0);
    word_index_set(next_wordindex, e_next_wordindex, line, linecode, 1);
    word_index_set(back_wordindex, e_back_wordindex, line, linecode, -1);

    /*コマンド格納*/
    comand.push(keycode);


    if (event.shiftKey){
      shiftbutton(keycode, line, row, linecode, linecount, end_row_position);

    } else if (event.ctrlKey){
      console.log("ctrl");

    } else {
      onebutton(keycode, p, line, row, linecode, linecount, end_row_position, wordindex, next_wordindex, back_wordindex, e_wordindex, e_next_wordindex);
    }

    return false;
  });
});



/*カーソルの移動制限判定*/
function move_control(v, p, line, row, linecode, linecount, end_row_position){

  var return_obj = new Object(); 
  switch (v){
    case 'up':
        /*カーソルが最上部の時*/
      if (p.top == 0){
        return_obj.l = '+=' + 0 + 'px'; 
        return_obj.r = '+=' + 0 + 'px';
        return return_obj; 

        /*カーソル位置の真上がnull（空白）の時*/
      } else if(row > end_row_position[line-1]) {
        return_obj.l = '-=' + lsize + 'px';
        if (end_row_position[line-1] == 0){
          return_obj.r = end_row_position[line-1]*rsize;
        } else {
          return_obj.r = end_row_position[line-1]*rsize - rsize;
        }
        if (!posmemory) posmemory = row;
        return return_obj;

        /*カーソル真上が存在して、なおかつposmemoryが存在して、posomemoryより上部の行が短い時*/
      } else if(row < end_row_position[line-1] && posmemory && posmemory > end_row_position[line-1]){
        return_obj.l = '-=' + lsize + 'px';
        return_obj.r = end_row_position[line-1]*rsize - rsize;
        return return_obj;

        /*カーソル真上が存在して、posmemoryも存在して、posmemoryより上部の行が長い時*/
      } else if(row < end_row_position[line-1] && posmemory && posmemory < end_row_position[line-1]){
        return_obj.l = '-=' + lsize + 'px';
        return_obj.r = posmemory*rsize - rsize;
        posmemory = null;
        return return_obj;
        
        /*普通の上移動*/
      } else {
        return_obj.l = '-=' + lsize + 'px';
        return_obj.r = '+=' + 0 + 'px';
        return return_obj;
      }


    case 'down':
      if (line == linecount){
        return_obj.l = '+=' + lsize + 'px'; 
        return_obj.r =  0 + 'px';
        return return_obj; 


       } else if(p.top == $('.play').height()-lsize){
        return_obj.l = '+=' + 0 + 'px'; 
        return_obj.r = '+=' + 0 + 'px';
        return return_obj; 

        /*カーソルの真下が空白の時（null)*/
       } else if(row > end_row_position[line+1]) {
        return_obj.l = '+=' + lsize + 'px';
        if (end_row_position[line+1] == 0){
          return_obj.r = end_row_position[line+1]*rsize;
        } else {
          return_obj.r = end_row_position[line+1]*rsize - rsize;
        }
        if (!posmemory) posmemory = row;
        return return_obj;

        /*カーソル真下が存在して、なおかつposmemoryが存在して、posomemoryより下部の行が短い時*/
      } else if(row < end_row_position[line+1] && posmemory && posmemory > end_row_position[line+1]){
        return_obj.l = '+=' + lsize + 'px';
        return_obj.r = end_row_position[line+1]*rsize - rsize;
        return return_obj;

        /*カーソル真下が存在して、posmemoryも存在して、posmemoryより下部の行が長い時*/
      } else if(row < end_row_position[line+1] && posmemory && posmemory <= end_row_position[line+1]){
        return_obj.l = '+=' + lsize + 'px';
        return_obj.r = posmemory*rsize - rsize;
        posmemory = null;
        return return_obj;
        
        /*普通の下移動*/
      } else {
        return_obj.l = '+=' + lsize + 'px';
        return_obj.r = '+=' + 0 + 'px';
        return return_obj;
      }
    
    case 'left':
      if(p.left == 0 && p.top == 0){
        return_obj.l = '+=' + 0 + 'px'; 
        return_obj.r = '+=' + 0 + 'px';
        posmemory = null;
        return return_obj; 
      }

      if (p.left == 0 && !linecode[line-1]){
        return_obj.l = '-=' + lsize + 'px'; 
        return_obj.r = '+=' + 0 + 'px';
        posmemory = null;
        return return_obj; 

      } else if (p.left == 0 && linecode[line-1]){
        return_obj.l = '-=' + lsize + 'px'; 
        return_obj.r = end_row_position[line-1]*rsize - rsize + 'px';
        posmemory = null;
        return return_obj; 
      } else {
        return_obj.l = '-=' + 0 + 'px'; 
        return_obj.r = '-=' + rsize + 'px';
        posmemory = null;
        return return_obj; 
      }



    case 'right':
      if (!linecode[line] && line <= linecount){
        return_obj.l = '+=' + lsize + 'px'; 
        return_obj.r =  0 + 'px';
        posmemory = null;
        return return_obj; 
      }

      if (row == end_row_position[line]){
        return_obj.l = '+=' + lsize + 'px'; 
        return_obj.r =  0 + 'px';
        posmemory = null;
        return return_obj; 


      }else if (line > linecount){
        return_obj.l = '+=' + 0 + 'px'; 
        return_obj.r = '+=' + 0 + 'px';
        posmemory = null;
        return return_obj;


      } else {
        return_obj.l = '+=' + 0 + 'px';
        return_obj.r = '+=' + rsize + 'px';
        posmemory = null;
        return return_obj;
      }
  }
}



/*キー１つの時の動作、h,j,k,l,w,b,0,^*/
function onebutton(keycode, p, line, row, linecode, linecount, end_row_position, wordindex, next_wordindex, back_wordindex, e_wordindex, e_next_wordindex){
  switch (keycode){

    case 76:/*key:l*/
      var mv = move_control('right', p, line, row, linecode, linecount, end_row_position);
      $('.cursor').animate({'top': mv.l, 'left': mv.r},10);
      comand.length = 0;
      break;

    case 72:/*key:h*/
      var mv = move_control('left', p, line, row, linecode, linecount, end_row_position);
      $('.cursor').animate({'top': mv.l, 'left': mv.r},10);
      comand.length = 0;
      break;

    case 74:/*key:j*/
      var mv = move_control('down', p, line, row, linecode, linecount, end_row_position);
      $('.cursor').animate({'top': mv.l, 'left': mv.r},10);
      comand.length = 0;
      break;
      
    case 75:/*key:k*/
      var mv = move_control('up', p, line, row, linecode, linecount, end_row_position);
      $('.cursor').animate({'top': mv.l, 'left': mv.r},10);
      comand.length = 0;
      break;

    case 87:/*key:w*/
      
      /*最下部の時のw*/
      if (p.top == $('.play').height()-lsize) break; 

      /*空白行にいる時のw*/
      if (wordindex.length == 1){
        if (next_wordindex.length == 1){
          $('.cursor').animate({ 'top': '+=' + lsize + 'px','left': 0 + 'px'},10);
        } else {
          $('.cursor').animate({ 'top': '+=' + lsize + 'px','left': next_wordindex[1]*rsize - rsize + 'px'},10);
        }
        break;
      }

      /*次の行が空白行の時のw*/
      if (row >= wordindex[wordindex.length-1] && next_wordindex.length == 1){
        $('.cursor').animate({ 'top': '+=' + lsize + 'px','left': 0 + 'px'},10);
        break;
      }

      win:
      for (var i = 1; i <= wordindex.length -1; i++){
        if (row >= wordindex[wordindex.length - 1]){
          $('.cursor').animate({ 'top': '+=' + lsize + 'px','left': next_wordindex[1]*rsize - rsize + 'px'},10);
          break win;
        } else if (row < wordindex[i]){
          $('.cursor').animate({ 'left': wordindex[i]*rsize - rsize + 'px'},10);
          break win;
        }
      }
      comand.length = 0;
      break;

    case 66:/*key:b*/
      /*最上部の時のb*/
      if (p.top == 0 && p.left == 0) break;

      /*前の行が空白行の時のb*/
      if (row <= wordindex[1] && back_wordindex.length == 1){
        $('.cursor').animate({ 'top': '-=' + lsize + 'px','left': 0 + 'px'},10);
        break;
      }


      /*空白行にいる時のb*/
      if (wordindex.length == 1){
        if (back_wordindex.length == 1){
          $('.cursor').animate({ 'top': '-=' + lsize + 'px','left': 0 + 'px'},10);
        } else {
          $('.cursor').animate({ 'top': '-=' + lsize + 'px','left': back_wordindex[back_wordindex.length - 1]*rsize - rsize + 'px'},10);
        }
        break;
      }

      /*通常のb*/
      wib:
      for (var i = 1; i <= wordindex.length -1; i++){
        if (row <= wordindex[1]){
          $('.cursor').animate({ 'top': '-=' + lsize + 'px','left': back_wordindex[back_wordindex.length - 1]*rsize - rsize + 'px'},10);
          break wib;
        } else if (row > wordindex[i] && row < wordindex[i+1]){
          $('.cursor').animate({ 'left': wordindex[i]*rsize - rsize + 'px'},10);
          break wib;
        } else if (row ==  wordindex[i]){
          $('.cursor').animate({ 'left': wordindex[i-1]*rsize - rsize + 'px'},10);
          break wib;
        }else if (row > wordindex[wordindex.length-1]){
          $('.cursor').animate({ 'left': wordindex[wordindex.length-2]*rsize - rsize + 'px'},10);
        }
      }
      comand.length = 0;
      break;

    case 48:/*key:0*/
      $('.cursor').animate({ 'left': 0 },10);
      comand.length = 0;
      break;

    case 187:/*key:^*/
      $('.cursor').animate({ 'left': wordindex[1]*rsize - rsize + 'px'},10);
      comand.length = 0;
      break;

    case 69:/*key:e*/
      /*最下部の時のe*/
      if (p.top == $('.play').height()-lsize) break; 

      /*最後の空白行のe*/
      if (line > linecount){
        $('.cursor').animate({'top': $('.play').height()-lsize + 'px','left': 0  + 'px'},10);
        break;
      }

      /*次の行が空白行の時のe*/
      if (row >= e_wordindex[e_wordindex.length-1] && next_wordindex.length == 1){
        $('.cursor').animate({ 'top': '+=' + lsize + 'px','left': 0 + 'px'},10);
        break;
      }

      /*空白行にいる時のe*/
      if (wordindex.length == 1){
        if (next_wordindex.length == 1){
          $('.cursor').animate({ 'top': '+=' + lsize + 'px','left': 0 + 'px'},10);
        } else {
          $('.cursor').animate({ 'top': '+=' + lsize + 'px','left': e_next_wordindex[1]*rsize - rsize + 'px'},10);
        }
        break;
      }

      /*通常のe*/
      wine:
      for (var i = 1; i <= e_wordindex.length -1; i++){
        if (row == e_wordindex[e_wordindex.length - 1]){
          $('.cursor').animate({ 'top': '+=' + lsize + 'px','left': e_next_wordindex[1]*rsize - rsize + 'px'},10);
          break wine;
        } else if (row < e_wordindex[i]){
          $('.cursor').animate({ 'left': e_wordindex[i]*rsize - rsize + 'px'},10);
          break wine;
        }
      }
      comand.length = 0;
      break;

    case 71:/*key:g*/
      if (comand.length == 2 && comand[0] == 71 && comand [1] == 71 ){
        comand.length = 0;
        $('.cursor').animate({ 'top': + 0 + 'px','left': +  0 + 'px'},10);
        break;
      } else if( comand[comand.length-2] != 71){
        comand.splice(0, comand.length -1);
        console.log(comand);
        break;
      }
      break;

    case 49:
    case 50:
    case 51:
    case 52:
    case 53:
    case 54:
    case 55:
    case 56:
    case 57:/*数字キー*/
      break;




    case 88:/*key:x*/
      if(!gamestart) break;
      for(var i = 0; i < mato; i++){
        
          if(p.top - 3 < $('span').eq(i).position().top && 
            p.top + 2 > $('span').eq(i).position().top && 
            $('span').eq(i).position().left > p.left -1 && 
            $('span').eq(i).position().left < p.left + 1 ){

              /*的を消して点数を表示*/
              $("span").eq(i).trigger("click");
              mato--;
              score += 10; 
              document.getElementById("score").innerHTML= "<p id = \"score\">" + "score:" + score + "</p>";
            
              /*まとが0になったらリセット*/
              if(mato == 0){
                mato = 15;
                set_mato();
              }
          }
      }
      break;

    default:
      comand.length = 0;
      break;
      





  }
}

/*shift + key */
function shiftbutton(keycode, line, row, linecode, linecount, end_row_position){
  var mv_line_num = 0; 

  if (event.shiftKey){

    switch (keycode){

      case 71:/*key:G*/
        console.log(comand);

        /*最後の行へ*/
        if (comand[comand.length-2]  == 16 && comand[comand.length-1] == 71 && isNaN(comand[comand.length-3])){
        $('.cursor').animate({ 'top': + $('.play').height()-lsize + 'px','left': +  0 + 'px'},10);
        comand.length = 0;

        /*数字の行へ*/
        } else if (comand[comand.length-2]  == 16 && comand[comand.length-1] == 71 && !isNaN(comand[comand.length-3])){
          comand.splice(comand.length-2, 1);

          for (var i = 0; i < comand.length -1; i ++){
            mv_line_num += String.fromCharCode(comand[i]);
          }

          if(mv_line_num <=linecount){
            $('.cursor').animate({ 'top':  mv_line_num*lsize - lsize + 'px','left': +  0 + 'px'},10);
          } else {
            $('.cursor').animate({ 'top': + $('.play').height()-lsize + 'px','left': +  0 + 'px'},10);
          }
          comand.length = 0;

          break;
        }
      
      case 16:/*key:shift*/
        break;

      case 52:/*key:$*/
        if(linecode[line] == "") break;
        $('.cursor').animate({ 'left': end_row_position[line]*rsize - rsize + 'px'},10);
        comand.length = 0;
        posmemory = null;
        break;
        
      default:
      comand.length = 0;
      break;
    }
  } else {
    console.log("話したな！");
  }
}




/*wordのインデックスをセット*/
function word_index_set(wordindex, e_wordindex, line, linecode, l){
    wordindex.length = 0;
    wordindex.push(0);
    e_wordindex.length = 0;
    e_wordindex.push(0);
    var wresult;
    while(wresult = regular_word.exec(linecode[line + l])){
      wordindex.push(wresult.index + 1); 
      e_wordindex.push(regular_word.lastIndex); 
    }
}








           



